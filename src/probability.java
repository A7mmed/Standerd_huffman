/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package standard_huffman;

/**
 *
 * @author Ahmed Ibrahim
 */
public class probability {
    
    public String ch;
    public int pro;
    public String dic = "";

    public probability() {
    }

    public probability(String ch,int pro) {
        this.ch = ch;
        this.pro = pro;
    }
    public void setdic(String x){
        dic =  x;
    }
    public String getdic()
    {
        return this.dic;
    }
    
    
}
