/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package standard_huffman;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.*;
import java.util.*;
import java.lang.*;
/**
 *
 * @author Ahmed Ibrahim
 */
public class Standard_huffman {
    Scanner in = new Scanner(System.in);


    FileOutputStream file ;
    DataInputStream dis;
    DataOutputStream dos;
    FileInputStream file2;
    Standard_huffman(){}
    //public ArrayList<probability>mm3 =  new ArrayList();
    public ArrayList<probability> code(ArrayList<probability> v)
    {
        ArrayList<ArrayList<probability>> arr = new ArrayList();
        arr.add(v);
        int count = 1;
        while(count < v.size()-1){
            ArrayList<probability> ar = new ArrayList();
            for(int i = 0;i< arr.get(count-1).size()-1;i++)
            {
                if(i == arr.get(count-1).size()-2)
                {
                    String a = arr.get(count-1).get(arr.get(count-1).size()-2).ch + arr.get(count-1).get(arr.get(count-1).size()-1).ch;
                    int aa =  arr.get(count-1).get(arr.get(count-1).size()-2).pro + arr.get(count-1).get(arr.get(count-1).size()-1).pro;
                    ar.add(new probability(a,aa));
                }
                else
                {
                    int z = arr.get(count-1).size() - arr.get(count-1).size() + i;
                    String a = arr.get(count-1).get(z).ch;
                    int aa = arr.get(count-1).get(z).pro;
                   ar.add(new probability(a,aa));
                }

            }
            for(int i =0;i<ar.size();i++){
                for(int j=0;j<i;j++)
                {
                if(ar.get(i).pro > ar.get(j).pro)
                {
                    String a1 = ar.get(i).ch;
                    int a2 = ar.get(i).pro;
                    ar.get(i).ch = ar.get(j).ch;
                    ar.get(i).pro = ar.get(j).pro;
                    ar.get(j).ch = a1;
                    ar.get(j).pro = a2;
                }
            }
        }
            arr.add(ar);
            count++;
        }
    while(count>0){
        if(arr.get(count-1).size()==2)
        {
            arr.get(count-1).get(arr.get(count-1).size()-1).setdic("1");
            arr.get(count-1).get(arr.get(count-1).size()-2).setdic("0");
        }
        else
        {
            String t = arr.get(count-1).get(arr.get(count-1).size()-2).ch;
            t=t+arr.get(count-1).get(arr.get(count-1).size()-1).ch;
            int z=0;
            for(int i = 0;i<arr.get(count).size();i++)
            {
                String n = arr.get(count).get(i).ch;
                if(n.equals(t)){
                    z = i;
                    break;
                }
            }
            String m = arr.get(count).get(z).getdic();
            arr.get(count-1).get(arr.get(count-1).size()-1).setdic(m + 1);
            arr.get(count-1).get(arr.get(count-1).size()-2).setdic(m + 0);
            for(int i =0;i<arr.get(count-1).size()-2;i++){
                String t1 = arr.get(count-1).get(i).ch;
                int z1=0;
                for(int j =0;j<arr.get(count).size();j++)
                {
                    String n = arr.get(count).get(j).ch;
                    if(n.equals(t1)){
                    z1 = j;

                }
                }
                String m1 = arr.get(count).get(z1).getdic();
                arr.get(count-1).get(i).setdic(m1);
            }
        }
        count--;
    }
    return arr.get(0);
    }
    public void compress(String f1,String f2,String f3) throws FileNotFoundException, IOException
    {
        String x;
        file2 = new FileInputStream(f1);
        dis = new DataInputStream(file2);
        x = dis.readLine();
        String s = "";
        String l="";
        ArrayList<probability> v = new ArrayList();

        for(int i = 0;i<x.length();i++){
            if(s.indexOf(x.charAt(i))!=-1)continue;
            else {
                s = s + x.charAt(i);
            }
        }
        for(int i = 0;i<s.length();i++)
        {
            int count =0;
            for(int j=0;j<x.length();j++){

                if(s.charAt(i)==x.charAt(j))
                    count++;
                }
            String n = "";
            n = n + s.charAt(i);
            v.add(new probability(n,count));
            }
        for(int i =0;i<v.size();i++){
            for(int j=0;j<i;j++)
            {
                if(v.get(i).pro > v.get(j).pro)
                {
                    String a1 = v.get(i).ch;
                    int a2 = v.get(i).pro;
                    v.get(i).ch = v.get(j).ch;
                    v.get(i).pro = v.get(j).pro;
                    v.get(j).ch = a1;
                    v.get(j).pro = a2;
                }
            }
        }
        ArrayList<probability> ar = new ArrayList();
        ar = code(v);
        for(int i =0;i<ar.size();i++){
            System.out.println(ar.get(i).ch);
            System.out.println(ar.get(i).getdic());
        }
    file = new FileOutputStream(f2);
            //FileWriter write = new FileWriter(com);
            //String buffer = "";
            dos = new DataOutputStream(file);
    for(int k=0;k<x.length();k++)
    {
        for(int j=0;j<ar.size();j++){
           String g = "";
            g = g + x.charAt(k);
            if(g.equals(ar.get(j).ch)){
                l = ar.get(j).getdic();
                break;
            }
        }
         try{
                dos.writeBytes(l);
                //dos.writeBytes(",");
            //dos.flush();

        }
        catch(FileNotFoundException e){
            System.out.println("error");
        }
        l = "";
    }
    file = new FileOutputStream(f3);
            //FileWriter write = new FileWriter(com);
            //String buffer = "";
            dos = new DataOutputStream(file);
            for(int i=0;i<ar.size();i++)
            {
                dos.writeBytes(ar.get(i).ch);
                dos.writeBytes(",");
                dos.writeBytes(ar.get(i).dic);
                dos.writeBytes(",");

            }

    //System.out.println(l);
    //mm3 = ar;
    }
    public void decompress(String f1,String f2 , String f3) throws FileNotFoundException, IOException{
        Scanner read2 = new Scanner (new File(f2));
        ArrayList<dicionary> f = new ArrayList();
        read2.useDelimiter(",");
        while(read2.hasNext())
        {
            String j = read2.next();
            String c = read2.next();
            dicionary k = new dicionary(j,c);
            f.add(k);
        }
        Scanner read = new Scanner (new File(f1));
        read.useDelimiter(",");
        String l = "";
        String h = "";
        while(read.hasNext())
        {
            String s = "";
            l = read.next();
            char[] c = l.toCharArray();
            for(int j =0;j<c.length;j++){

            s = s + c[j];
            for(int i =0;i<f.size();i++)
            {
                if(s.equals(f.get(i).code))
                {
                    h = h + f.get(i).ch;
                    s = "";
                }

            }
            }
        }
        System.out.println(h);
        file = new FileOutputStream(f3);
        dos = new DataOutputStream(file);
        dos.writeBytes(h);

    }
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
       new huffmanGUI().setVisible(true);
    }

}
